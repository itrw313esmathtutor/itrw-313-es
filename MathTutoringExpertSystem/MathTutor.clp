(deftemplate mathTutor (slot numbers2)
(slot numbers3)
(slot numbers4)
(slot operatorAddition)
(slot operatorSubtraction)
(slot operatorDivision)
(slot operatorMultiplication) 
(slot questions10)
(slot questions25)
(slot difficultyEasy)
(slot difficultyHard))

(deffacts m-Details (mathTutor
(numbers2 2)
(numbers3 3)
(numbers4 4)
(operatorAddition Add)
(operatorSubtraction Sub)
(operatorDivision Div)
(operatorMultiplication Multi)
(questions10 10)
(questions25 25)
(difficultyEasy Easy)
(difficultyHard Hard)))

(defrule addition-1
	(mathTutor(operatorAddition ?a_value)
		  (numbers2 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A210QE.txt" computeData.dat "a")

(close))



(defrule addition-2
	(mathTutor(operatorAddition ?a_value)
		  (numbers2 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A225QE.txt" computeData "a")

(close))

(defrule addition-3
	(mathTutor(operatorAddition ?a_value)
		  (numbers3 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A310QE.txt" computeData "a")

(close))

(defrule addition-4
	(mathTutor(operatorAddition ?a_value)
		  (numbers3 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A325QE.txt" computeData "a")

(close))


(defrule addition-5
	(mathTutor(operatorAddition ?a_value)
		  (numbers4 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A410QE.txt" computeData "a")

(close))

(defrule addition-6
	(mathTutor(operatorAddition ?a_value)
		  (numbers4 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A425QE.txt" computeData "a")

(close))

(defrule addition-7
	(mathTutor(operatorAddition ?a_value)
		  (numbers2 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A210QH.txt" computeData "a")


(close))


(defrule addition-8
	(mathTutor(operatorAddition ?a_value)
		  (numbers2 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A225QH.txt" computeData "a")

(close))

(defrule addition-9
	(mathTutor(operatorAddition ?a_value)
		  (numbers3 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A310QH.txt" computeData "a")

(close))

(defrule addition-10
	(mathTutor(operatorAddition ?a_value)
		  (numbers3 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A325QH.txt" computeData "a")

(close))

(defrule addition-11
	(mathTutor(operatorAddition ?a_value)
		  (numbers4 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A410QH.txt" computeData "a")

(close))

(defrule addition-12
	(mathTutor(operatorAddition ?a_value)
		  (numbers4 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "A425QH.txt" computeData "a")

(close))


(defrule subtract-1
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers2 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S210QE.txt" computeData "a")


(close))


(defrule subtract-2
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers2 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S225QE.txt" computeData "a")

(close))



(defrule subtract-3
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers3 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S310QE.txt" computeData "a")

(close))

(defrule subtract-4
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers3 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S325QE.txt" computeData "a")

(close))


(defrule subtract-5
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers4 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S410QE.txt" computeData "a")

(close))

(defrule subtract-6
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers4 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S425QE.txt" computeData "a")

(close))


(defrule subtract-7
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers2 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S210QH.txt" computeData "a")


(close))


(defrule subtract-8
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers2 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S225QH.txt" computeData "a")

(close))


(defrule subtract-9
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers3 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S310QH.txt" computeData "a")

(close))

(defrule subtract-10
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers3 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S325QH.txt" computeData "a")

(close))

(defrule subtract-11
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers4 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S410QH.txt" computeData "a")

(close))

(defrule subtract-12
	(mathTutor(operatorSubtraction ?a_value)
		  (numbers4 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "S425QH.txt" computeData "a")

(close))

(defrule multiply-1
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers2 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M210QE.txt" computeData "a")


(close))


(defrule multiply-2
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers2 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M225QE.txt" computeData "a")

(close))

(defrule multiply-3
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers3 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M310QE.txt" computeData "a")

(close))

(defrule multiply-4
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers3 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M325QE.txt" computeData "a")

(close))

(defrule multiply-5
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers4 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M410QE.txt" computeData "a")

(close))

(defrule multiply-6
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers4 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M425QE.txt" computeData "a")

(close))


(defrule multiply-7
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers2 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M210QH.txt" computeData "a")


(close))


(defrule multiply-8
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers2 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M225QH.txt" computeData "a")

(close))

(defrule multiply-9
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers3 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M310QH.txt" computeData "a")

(close))

(defrule multiply-10
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers3 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M325QH.txt" computeData "a")

(close))

(defrule multiply-11
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers4 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M410QH.txt" computeData "a")

(close))

(defrule multiply-12
	(mathTutor(operatorMultiplication ?a_value)
		  (numbers4 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "M425QH.txt" computeData "a")

(close))

(defrule division-1
	(mathTutor(operatorDivision ?a_value)
		  (numbers2 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D210QE.txt" computeData "a")


(close))


(defrule division-2
	(mathTutor(operatorDivision ?a_value)
		  (numbers2 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D225QE.txt" computeData "a")

(close))

(defrule division-3
	(mathTutor(operatorDivision ?a_value)
		  (numbers3 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D310QE.txt" computeData "a")

(close))

(defrule division-4
	(mathTutor(operatorDivision ?a_value)
		  (numbers3 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D325QE.txt" computeData "a")

(close))

(defrule division-5
	(mathTutor(operatorDivision ?a_value)
		  (numbers4 ?n_value)
		  (questions10 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D410QE.txt" computeData "a")

(close))

(defrule division-6
	(mathTutor(operatorDivision ?a_value)
		  (numbers4 ?n_value)
		  (questions25 ?q_value)
		  (difficultyEasy ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D425QE.txt" computeData "a")

(close))

(defrule division-7
	(mathTutor(operatorDivision ?a_value)
		  (numbers2 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D210QH.txt" computeData "a")


(close))


(defrule division-8
	(mathTutor(operatorDivision ?a_value)
		  (numbers2 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D225QH.txt" computeData "a")

(close))

(defrule division-9
	(mathTutor(operatorDivision ?a_value)
		  (numbers3 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D310QH.txt" computeData "a")

(close))

(defrule division-10
	(mathTutor(operatorDivision ?a_value)
		  (numbers3 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D325QH.txt" computeData "a")

(close))

(defrule division-11
	(mathTutor(operatorDivision ?a_value)
		  (numbers4 ?n_value)
		  (questions10 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D410QH.txt" computeData "a")

(close))

(defrule division-12
	(mathTutor(operatorDivision ?a_value)
		  (numbers4 ?n_value)
		  (questions25 ?q_value)
		  (difficultyHard ?d_value))
=>
(printout t ?a_value "  " ?n_value "  " ?q_value "  " ?d_value " " crlf)

(open "D425QH.txt" computeData "a")
(close))
